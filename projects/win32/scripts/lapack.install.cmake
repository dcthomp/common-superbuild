file(INSTALL "${source_dir}/bin/" DESTINATION "${install_dir}/bin")
file(INSTALL "${source_dir}/include/" DESTINATION "${install_dir}/include")
file(INSTALL "${source_dir}/lib/" DESTINATION "${install_dir}/lib")
file(INSTALL "${source_dir}/share/" DESTINATION "${install_dir}/share")
